# Generated from ascii_binder-0.1.10.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name ascii_binder

Name: rubygem-%{gem_name}
Version: 0.1.14
Release: 2%{?dist}
Summary: An AsciiDoc-based system for authoring and publishing documentation
License: MIT
URL: http://asciibinder.org/
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(asciidoctor)
BuildRequires: %{_bindir}/cucumber
BuildRequires: rubygem(git)
BuildRequires: rubygem(diff_dirs) >= 0.1.2
BuildRequires: rubygem(sitemap_generator)
BuildRequires: rubygem(tilt)
BuildRequires: rubygem(trollop) >= 2.1.2
BuildArch: noarch

%description
AsciiBinder is an AsciiDoc-based system for authoring and publishing closely
related documentation sets from a single source.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n  %{gem_name}-%{version}

# Fix the broken link.
# https://github.com/redhataccess/ascii_binder/issues/64
# https://github.com/rubygems/rubygems/issues/2007
ln -sf asciibinder bin/ascii_binder

# Relax sitemap_generator dependency.
%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g sitemap_generator '~> 5.1.0'
%gemspec_add_dep -s ../%{gem_name}-%{version}.gemspec -g sitemap_generator '>= 5.1.0'

# The asciidoctor-diagram is not in Fedora yet.
%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g asciidoctor-diagram '~> 1.5.5'
sed -i '/asciidoctor-diagram/ s/^/#/' lib/ascii_binder/engine.rb

%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g haml '>= 0'
%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g guard-livereload '>= 0'
%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g rake '~> 10.0'
%gemspec_remove_dep -s ../%{gem_name}-%{version}.gemspec -g yajl-ruby '~> 1.3.0'

sed -i "/guard 'livereload' do/,/^end$/ s/^/#/" lib/ascii_binder/tasks/guards.rb

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

# Fix shebang.
sed -i 's|/usr/bin/env ruby|/usr/bin/ruby|' %{buildroot}%{gem_instdir}/bin/asciibinder

%check
git config --global user.email "you@example.com"
git config --global user.name "Your Name"

pushd .%{gem_instdir}
RUBYOPT=-Ilib cucumber
popd

%files
%dir %{gem_instdir}
# TODO: Disabled due to RubyGems issues, see the notes in %%prep section.
# %%{_bindir}/ascii_binder
%{_bindir}/asciibinder
%exclude %{gem_instdir}/.gitignore
%exclude %{gem_instdir}/.travis.yml
%{gem_instdir}/Dockerfile
%{gem_instdir}/Guardfile
%license %{gem_instdir}/LICENSE.txt
%dir %{gem_instdir}/bin
%{gem_instdir}/bin/asciibinder
# TODO: Excluded due to RubyGems issues, see the notes in %%prep section.
%exclude %{gem_instdir}/bin/ascii_binder
%{gem_libdir}
%{gem_instdir}/templates
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.adoc
%{gem_instdir}/Rakefile
%{gem_instdir}/ascii_binder.gemspec
%{gem_instdir}/features

%changelog
* Sun Jan 28 2018 Vít Ondruch <vondruch@redhat.com> - 0.1.14-2
- Enable guard-shell dependency, since it is already available in Fedora.

* Thu Jan 11 2018 Vít Ondruch <vondruch@redhat.com> - 0.1.14-1
- Update to AsciiBinder 0.1.14.

* Thu Dec 14 2017 Vít Ondruch <vondruch@redhat.com> - 0.1.13-1
- Update to AsciiBinder 0.1.13.

* Fri Sep 08 2017 Vít Ondruch <vondruch@redhat.com> - 0.1.10.1-1
- Initial package
